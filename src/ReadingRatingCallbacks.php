<?php

namespace Drupal\reading_rating;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Trusted render callbacks.
 */
class ReadingRatingCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['readingRatingPreRender'];
  }

  /**
   * Pre render function to set reading rating attributes.
   *
   * @param array|mixed $element
   *   The render array.
   *
   * @return array
   *   The processed render array.
   */
  public static function readingRatingPreRender($element) {
    if (isset($element['#enable_reading_rating']) && $element['#enable_reading_rating'] === TRUE) {
      $element['#attributes']['class'][] = 'reading-rating';
      $element['#attached']['library'][] = 'reading_rating/reading_rating';
    }
    return $element;
  }

}
