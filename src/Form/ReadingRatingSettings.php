<?php

namespace Drupal\reading_rating\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the reading rating form interface.
 */
class ReadingRatingSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reading_rating_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reading_rating.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('reading_rating.settings');

    $form['text_replacements'] = [
      '#type' => 'details',
      '#title' => $this->t('Text replacements'),
      '#open' => TRUE,
    ];

    $form['text_replacements']['rating_empty'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Empty rating'),
      '#description' => $this->t('Rating text when textarea is empty'),
      '#default_value' => $config->get('text_replacements.rating_empty') ?? $this->t('Unknown'),
      '#required' => TRUE,
    ];

    $form['text_replacements']['rating_easy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Easy rating'),
      '#default_value' => $config->get('text_replacements.rating_easy') ?? $this->t('Easy'),
      '#required' => TRUE,
    ];

    $form['text_replacements']['rating_moderate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Moderate rating'),
      '#default_value' => $config->get('text_replacements.rating_moderate') ?? $this->t('Moderate'),
      '#required' => TRUE,
    ];

    $form['text_replacements']['rating_difficult'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Difficult rating'),
      '#default_value' => $config->get('text_replacements.rating_difficult') ?? $this->t('Difficult'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('reading_rating.settings')
      // Set the submitted configuration setting.
      ->set('text_replacements.rating_empty', $form_state->getValue('rating_empty'))
      ->set('text_replacements.rating_easy', $form_state->getValue('rating_easy'))
      ->set('text_replacements.rating_moderate', $form_state->getValue('rating_moderate'))
      ->set('text_replacements.rating_difficult', $form_state->getValue('rating_difficult'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
