(function drupal(Drupal) {
  Drupal.behaviors.readingRating = {
    attach: function attach(context, settings) {
      once('readingrating', '.reading-rating').forEach(function (element) {
        rrInit(element);

        /**
         * Wait for CKEditor5 and get ID, if not, this is a standard text
         * field where we only need the value.
         */
        setTimeout(function () {
          const ckEditorId = element.dataset.ckeditor5Id;
          if (ckEditorId) {
            rrCkeditor5(Drupal.CKEditor5Instances.get(ckEditorId));
          } else {
            rrUpdateStats(element, element.value);
            ['keyup', 'change', 'paste'].forEach(function (evt) {
              element.addEventListener(evt, function () {
                rrUpdateStats(element, element.value);
              });
            });
          }
        });
      });

      // Initializes reading rating markup below text fields.
      function rrInit(element) {
        const appended = document.createElement('div');
        appended.classList.add('rr-container');
        appended.dataset.textFieldId = element.id;

        const textReplacements = settings.reading_rating.text_replacements;

        // @codingStandardsIgnoreStart
        appended.innerHTML = `
          <section class="rr-container--inner">
            <div class="rr-wrapper-label-ratings">
              <div class="rr-label">Reading Score:</div>
              <div class="rr-rating">
                <ul class="rr-rating-list">
                  <li data-rr-option="unknown" class="" aria-current="true">${textReplacements.rating_empty}</li>
                  <li data-rr-option="easy" class="animate__animated" aria-current="false">${textReplacements.rating_easy}</li>
                  <li data-rr-option="moderate" class="animate__animated " aria-current="false">${textReplacements.rating_moderate}</li>
                  <li data-rr-option="difficult" class="animate__animated " aria-current="false">${textReplacements.rating_difficult}</li>
              </div>
            </div>
             <div class="rr-help-text">Calculated using the <a href="https://en.wikipedia.org/wiki/Flesch–Kincaid_readability_tests" target="_blank">Flesch Reading Ease Score</a></div>
          </section>`;
        // @codingStandardsIgnoreEnd
        element.after(appended);
      }

      // Strips HTML and newline tags.
      function stripTags(input) {
        const normalizedText = input
          .replace(/(<([^>]+)>)/gi, ' ') // Replace HTML tags
          .replace(/(\r\n|\n|\r)/gm, ' ') // Replace new lines (in many forms)
          .replace(/^\s+|\s+$/g, ' ') // Replace leading or trailing multiple spaces
          .replace('&nbsp;', ' '); // Replaces most non-breaking-spaces

        return normalizedText;
      }

      function rrUpdateStats(textArea, text) {
        let target = null;
        const targetContainer =
          textArea.parentNode.getElementsByClassName('rr-container')[0];
        const ratingListLabels = Array.from(
          targetContainer.querySelectorAll('[data-rr-option]'),
        );
        const stripped = stripTags(text);

        if (!stripped) {
          target = targetContainer.querySelector('[data-rr-option="unknown"]');
        } else if (typeof textstatistics !== 'undefined') {
          const textStats = new textstatistics();
          const rating = textStats.fleschKincaidReadingEase(stripped);

          switch (true) {
            case rating < 50:
              target = targetContainer.querySelector(
                '[data-rr-option="difficult"]',
              );
              break;

            case rating < 60:
              target = targetContainer.querySelector(
                '[data-rr-option="moderate"]',
              );
              break;

            default:
              target = targetContainer.querySelector('[data-rr-option="easy"]');
          }
        } else {
          console.warn(
            "The text-statistics.js library did not load properly or doesn't exist.",
          );
        }

        // Update rating label.
        if (target) {
          ratingListLabels.forEach(function (key) {
            key.setAttribute('aria-current', false);
          });
          target.setAttribute('aria-current', true);
        }
      }

      function rrCkeditor5(editor) {
        once('readingratingckeditor', editor.sourceElement).forEach(
          function (ckEditorTextArea) {
            let textData = editor.getData();
            rrUpdateStats(ckEditorTextArea, textData);

            ['keyup', 'change', 'paste'].forEach(function (evt) {
              editor.model.document.on(evt, function () {
                textData = editor.getData();
                rrUpdateStats(ckEditorTextArea, textData);
              });
            });
          },
        );
      }
    },
  };
})(Drupal);
