# Reading Rating
## Description

This Drupal module provides the ability to rate CKEditor5 and standard text area fields for readability using the [Flesch-Kincaid](https://en.wikipedia.org/wiki/Flesch–Kincaid_readability_tests) readability test. Currently, scores of readability are marked "Easy", "Moderate", or "Difficult".

## Project Page

* [Drupal.org project page](https://www.drupal.org/project/reading_rating)
* [Issue Queue](hhttps://www.drupal.org/project/issues/search/reading_rating) - Please submit feature requests, bugs, and feedback here.

## Requirements

- Drupal 10
- Out of the box supported text fields
  - CKEditor5
  - Standard text area fields - i.e. Text (plain, long)
## Installation

- Install the module using composer: `composer require drupal/reading_rating`
- Enable the module at `/admin/modules`
- Visit the form display for a content type, block type, paragraph, etc.
- Click on the gear icon to the right of a supported text field.
- At the bottom of the field settings, open "Reading Rating Settings" details.
- Tick the checkbox next to "Enable Reading Rating".
- Update the field settings by clicking "Update".
- Save the form display.
- Visit the edit page which includes the field that was just edited.
- There is now a "Reading Rating" section below the text field.
- As the editor types in the text field, the rating updates in real-time.

## Configurable and translatable rating strings

- Rating text is now configurable at `/admin/config/content/reading-rating`.
- To access this area a user must have `manage reading rating` permissions.
- These text strings are also translatable
  using Drupal core `config_translation` module.


## API
The hook `hook_reading_rating_widget_settings()` allows
for adding additional text fields.
See `reading_rating.api.php` for usage.
## Planned Features

- Add additional readability algorithms.
- Add customizable grade letter based rating.
- Allow summary fields to be rated.

## Credits

* [Text Statistics](https://github.com/cgiffard/TextStatistics.js) - Main JavaScript library used to parse text
* [MaxLength](https://www.drupal.org/project/maxlength) - Inspiration

## Maintainers

* [Marc Berger (codechefmarc)](https://www.drupal.org/u/codechefmarc)

## Supporting Organizations

* [Four Kitchens](https://www.fourkitchens.com)
